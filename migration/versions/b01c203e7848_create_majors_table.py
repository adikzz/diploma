"""create_majors_table

Revision ID: b01c203e7848
Revises: 
Create Date: 2022-06-01 04:40:09.893385

"""
from alembic import op
from sqlalchemy import Column, String


# revision identifiers, used by Alembic.
revision = 'b01c203e7848'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('majors',
                    Column('code', String(7), primary_key=True, index=True),
                    Column('name', String(255)),
                    )


def downgrade():
    op.drop_table('majors')
