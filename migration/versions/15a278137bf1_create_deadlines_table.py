"""create_deadlines_table

Revision ID: 15a278137bf1
Revises: 9c8fa9425280
Create Date: 2022-06-01 04:41:43.660638

"""
from alembic import op
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String


# revision identifiers, used by Alembic.
revision = '15a278137bf1'
down_revision = '9c8fa9425280'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('deadlines',
                    Column('id', Integer, primary_key=True, index=True),
                    Column('group_name', String(255), ForeignKey('groups.name')),
                    Column('start_time', DateTime(timezone=True)),
                    Column('end_time', DateTime(timezone=True))
                    )


def downgrade():
    op.drop_table('deadlines')