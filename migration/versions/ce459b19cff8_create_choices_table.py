"""create_choices_table

Revision ID: ce459b19cff8
Revises: bf21f771b455
Create Date: 2022-06-01 04:41:14.327633

"""
from alembic import op
from sqlalchemy import Column, Integer, DateTime, ForeignKey, String
from sqlalchemy.sql import func


# revision identifiers, used by Alembic.
revision = 'ce459b19cff8'
down_revision = 'bf21f771b455'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('choices',
                    Column('id', Integer, primary_key=True, index=True),
                    Column('student_id', Integer, ForeignKey("users.id")),
                    Column('course_id', Integer, ForeignKey("courses.id")),
                    Column('priority', Integer),
                    Column('choice_date', DateTime(timezone=True),
                           server_default=func.now()),
                    Column('group_name', String(255), ForeignKey('groups.name'))
                    )


def downgrade():
    op.drop_table('choices')
