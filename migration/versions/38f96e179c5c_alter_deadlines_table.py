"""alter_deadlines_table

Revision ID: 38f96e179c5c
Revises: 15a278137bf1
Create Date: 2022-06-03 03:34:31.129228

"""
from alembic import op
from sqlalchemy import Column, Date


# revision identifiers, used by Alembic.
revision = '38f96e179c5c'
down_revision = '15a278137bf1'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('deadlines', Column('enrollment_date', Date))


def downgrade():
    op.drop_column('deadlines', 'enrollment_date')
