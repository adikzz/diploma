"""create_courses_table

Revision ID: bf21f771b455
Revises: 0d98cd6ec795
Create Date: 2022-06-01 04:41:03.461930

"""
from alembic import op
from sqlalchemy import Column, ForeignKey, String, Integer, Text, DateTime, ARRAY
from sqlalchemy.sql import func


# revision identifiers, used by Alembic.
revision = 'bf21f771b455'
down_revision = '0d98cd6ec795'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('courses',
                    Column('id', Integer, primary_key=True, index=True),
                    Column('name', String(255)),
                    Column('description', Text),
                    Column('purpose', Text),
                    Column('main_sections', Text),
                    Column('prerequisites', Text),
                    Column('postrequisites', Text),
                    Column('credit_number', Integer),
                    Column('student_number', Integer),
                    Column('syllabus', Text),
                    Column('teachers', ARRAY(String)),
                    Column('group_name', String(255), ForeignKey('groups.name'))
                    )


def downgrade():
    op.drop_table('courses')
