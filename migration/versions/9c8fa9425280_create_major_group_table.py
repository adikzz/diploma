"""create_major_group_table

Revision ID: 9c8fa9425280
Revises: ce459b19cff8
Create Date: 2022-06-01 04:41:29.937647

"""
from alembic import op
from sqlalchemy import Column, Integer, ForeignKey, String


# revision identifiers, used by Alembic.
revision = '9c8fa9425280'
down_revision = 'ce459b19cff8'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('major_group',
                    Column('id', Integer, primary_key=True, index=True),
                    Column('major_code', String(7), ForeignKey("majors.code")),
                    Column('group_name', String(255), ForeignKey("groups.name")),
                )


def downgrade():
    op.drop_table('major_group')
