"""create_users_table

Revision ID: 0d98cd6ec795
Revises: 15d524805a1b
Create Date: 2022-06-01 04:40:45.784368

"""
from alembic import op
from sqlalchemy import Column, String, Integer, Text, Boolean, Date, ForeignKey


# revision identifiers, used by Alembic.
revision = '0d98cd6ec795'
down_revision = '15d524805a1b'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('users',
                    Column('id', Integer, primary_key=True, index=True),
                    Column('iin', String(12)),
                    Column('second_name', String(255)),
                    Column('first_name', String(255)),
                    Column('middle_name', String(255)),
                    Column('email', String(255)),
                    Column('password', Text),
                    Column('group', String(255)),
                    Column('major_code', String(7), ForeignKey("majors.code")),
                    Column('photo_url', Text),
                    Column('enrollment_date', Date),
                    Column('role', String(255)),
                    Column('has_chosen', Boolean),
                    Column('is_active', Boolean),
                    )


def downgrade():
    op.drop_table('users')
