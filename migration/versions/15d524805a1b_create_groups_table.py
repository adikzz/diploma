"""create_groups_table

Revision ID: 15d524805a1b
Revises: b01c203e7848
Create Date: 2022-06-01 04:40:27.310272

"""
from alembic import op
from sqlalchemy import Column, String, Integer, Text


# revision identifiers, used by Alembic.
revision = '15d524805a1b'
down_revision = 'b01c203e7848'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('groups',
                    Column('name', String(255), primary_key=True, index=True),
                    Column('description', Text)
                    )


def downgrade():
    op.drop_table('groups')
