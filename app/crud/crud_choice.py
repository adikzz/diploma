from typing import Any, Dict, Optional, Union

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.user import User
from app.models.choice import Choice
from app.schemas.user import UserRole
from app.schemas.choice import ChoiceCreate, ChoiceUpdate


class CRUDChoice(CRUDBase[Choice, ChoiceCreate, ChoiceUpdate]):
    def is_privileged(self, user: User) -> bool:
        return user.role == UserRole.ADMIN


choice = CRUDChoice(Choice)
