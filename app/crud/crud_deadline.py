from typing import Any, Dict, Optional, Union

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.user import User
from app.models.deadline import Deadline
from app.schemas.user import UserRole
from app.schemas.deadline import DeadlineCreate, DeadlineCreate


class CRUDDeadline(CRUDBase[Deadline, DeadlineCreate, DeadlineCreate]):
    def is_privileged(self, user: User) -> bool:
        return user.role == UserRole.ADMIN


deadline = CRUDDeadline(Deadline)
