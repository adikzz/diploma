from typing import Any, Dict, Optional, Union, List
from sqlalchemy.orm import Session
from app.crud.base import CRUDBase
from app.models.user import User
from app.models.major_group import MajorGroup
from app.schemas.user import UserRole
from app.schemas.major_group import MajorGroupCreate, MajorGroupUpdate
from sqlalchemy.orm import load_only


class CRUDMajorGroup(CRUDBase[MajorGroup, MajorGroupCreate, MajorGroupUpdate]):
    def is_privileged(self, user: User) -> bool:
        return user.role == UserRole.ADMIN

    def get_major_by_group_name(self, db: Session, *, group_name: str) -> Optional[List[str]]:
        return db.query(MajorGroup).filter(MajorGroup.group_name == group_name).options(load_only(MajorGroup.major_code)).all()


major_group = CRUDMajorGroup(MajorGroup)
