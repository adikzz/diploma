from typing import Any, Dict, Optional, Union

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.user import User
from app.models.group import Group
from app.schemas.user import UserRole
from app.schemas.group import GroupCreate, GroupUpdate


class CRUDGroup(CRUDBase[Group, GroupCreate, GroupUpdate]):
    def get(self, db: Session, *, name: str) -> Optional[Group]:
        return db.query(Group).filter(Group.name == name).first()

    def remove(self, db: Session, *, name: str):
        obj = db.query(self.model).get(name)
        db.delete(obj)
        db.commit()
        return obj

    def is_privileged(self, user: User) -> bool:
        return user.role == UserRole.ADMIN


group = CRUDGroup(Group)
