from typing import Any, Dict, Optional, Union, List

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.user import User
from app.models.major import Major
from app.schemas.user import UserRole
from app.schemas.major import MajorCreate, MajorUpdate


class CRUDMajor(CRUDBase[Major, MajorCreate, MajorUpdate]):
    def get(self, db: Session, *, code: str) -> Optional[Major]:
        return db.query(Major).filter(Major.code == code).first()

    def remove(self, db: Session, *, code: str):
        obj = db.query(self.model).get(code)
        db.delete(obj)
        db.commit()
        return obj

    def is_privileged(self, user: User) -> bool:
        return user.role == UserRole.ADMIN


major = CRUDMajor(Major)
