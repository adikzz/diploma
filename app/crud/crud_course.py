from typing import Any, Dict, Optional, Union, List

from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.user import User
from app.models.course import Course
from app.schemas.user import UserRole
from app.schemas.course import CourseCreate, CourseUpdate


class CRUDCourse(CRUDBase[Course, CourseCreate, CourseUpdate]):    
    def get_by_group_name(
        self, 
        db: Session, 
        *, 
        group_name: str, 
        skip: int = 0, 
        limit: int = 30
    ) -> Optional[Course]:
        return db.query(Course).filter(Course.group_name == group_name).all()

    def is_privileged(self, user: User) -> bool:
        return user.role == UserRole.ADMIN


course = CRUDCourse(Course)
