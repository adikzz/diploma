import imp
from .crud_user import user
from .crud_choice import choice
from .crud_course import course
from .crud_major import major
from .crud_major_group import major_group
from .crud_group import group
from .crud_deadline import deadline