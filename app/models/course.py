from sqlalchemy import Column, Integer, String, Text, ForeignKey, ARRAY
from sqlalchemy.orm import relationship
from app.db.base import Base


class Course(Base):
    __tablename__ = "courses"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(255))
    description = Column(Text)
    purpose = Column(Text)
    main_sections = Column(Text)
    prerequisites = Column(Text)
    postrequisites = Column(Text)
    credit_number = Column(Integer)
    student_number = Column(Integer)
    syllabus = Column(Text)
    teachers = Column(ARRAY(String))
    group_name = Column(String(255), ForeignKey('groups.name'))

    choices = relationship("Choice", back_populates="course")
    group = relationship("Group", back_populates="courses")
