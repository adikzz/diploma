from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from app.db.base import Base


class Major(Base):
    __tablename__ = "majors"
    code = Column(String(7), primary_key=True, index=True)
    name = Column(String(255))

    major_group = relationship("MajorGroup", back_populates="major")
    users = relationship("User", back_populates="major")
