from sqlalchemy import Column, Integer, ForeignKey, DateTime, String, Date
from sqlalchemy.orm import relationship
from app.db.base import Base


class Deadline(Base):
    __tablename__ = "deadlines"
    id = Column(Integer, primary_key=True, index=True)
    group_name = Column(String(255), ForeignKey('groups.name'))
    start_time = Column(DateTime(timezone=True))
    end_time = Column(DateTime(timezone=True))
    enrollment_date = Column(Date)

    group = relationship("Group", back_populates="deadlines")
