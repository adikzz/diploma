from sqlalchemy import Column, String, Text
from sqlalchemy.orm import relationship
from app.db.base import Base


class Group(Base):
    __tablename__ = "groups"
    name = Column(String(255), primary_key=True, index=True)
    description = Column(Text)

    major_group = relationship("MajorGroup", back_populates="group")
    deadlines = relationship("Deadline", back_populates="group")
    courses = relationship("Course", back_populates="group")
    choices = relationship("Choice", back_populates="group")

