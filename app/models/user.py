from sqlalchemy import Column, Integer, Date, String, Text, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from app.db.base import Base


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True, index=True)
    iin = Column(String(12))
    second_name = Column(String(255))
    first_name = Column(String(255))
    middle_name = Column(String(255))
    email = Column(String(255))
    password = Column(Text)
    group = Column(String(255))
    major_code = Column(String(7), ForeignKey('majors.code'))
    photo_url = Column(Text)
    enrollment_date = Column(Date)
    role = Column(String(255))
    is_active = Column(Boolean)
    has_chosen = Column(Boolean)

    choices = relationship("Choice", back_populates="user")
    major = relationship("Major", back_populates="users")
