from .group import Group
from .course import Course
from .choice import Choice
from .user import User
from .major import Major
from .major_group import MajorGroup
from .deadline import Deadline