from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship
from app.db.base import Base


class MajorGroup(Base):
    __tablename__ = "major_group"
    id = Column(Integer, primary_key=True, index=True)
    major_code = Column(String(7), ForeignKey('majors.code'))
    group_name = Column(String(255), ForeignKey('groups.name'))

    group = relationship("Group", back_populates="major_group")
    major = relationship("Major", back_populates="major_group")
