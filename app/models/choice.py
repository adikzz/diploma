from sqlalchemy import Column, Integer, ForeignKey, DateTime, func, String
from app.db.base import Base
from sqlalchemy.orm import relationship


class Choice(Base):
    __tablename__ = "choices"
    id = Column(Integer, primary_key=True, index=True)
    student_id = Column(Integer, ForeignKey('users.id'))
    course_id = Column(Integer, ForeignKey('courses.id'))
    priority = Column(Integer)
    choice_date = Column(DateTime(timezone=True), server_default=func.now())
    group_name = Column(String(255), ForeignKey('groups.name'))

    user = relationship("User", back_populates="choices")
    course = relationship("Course", back_populates="choices")
    group = relationship("Group", back_populates="choices")
