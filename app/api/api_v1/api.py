from fastapi import APIRouter

from app.api.api_v1.endpoints import (
    courses,
    choices,
    login,
    major_group,
    users,
    major,
    deadline,
    group
)

api_router = APIRouter()
api_router.include_router(login.router, tags=["login"])
api_router.include_router(users.router, prefix="/users", tags=["users"])
api_router.include_router(courses.router, prefix="/courses", tags=["courses"])
api_router.include_router(choices.router, prefix="/choices", tags=["choices"])
api_router.include_router(major.router, prefix="/majors", tags=["majors"])
api_router.include_router(major_group.router, prefix="/major_group", tags=["major_group"])
api_router.include_router(deadline.router, prefix="/deadline", tags=["deadline"])
api_router.include_router(group.router, prefix="/group", tags=["group"])