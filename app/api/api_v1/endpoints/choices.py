from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.Choice)
def create_choice(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_user),
    choice_in: schemas.ChoiceCreate
) -> Any:
    choice = crud.choice.create(db, obj_in=choice_in)
    return choice


@router.get("/", response_model=List[schemas.Choice])
def read_choices(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 30,
    current_user: models.User = Depends(deps.get_current_active_user)
) -> Optional[Any]:
    choices = crud.choice.get_multi(db, skip=skip, limit=limit)
    return choices


@router.delete("/{id}", response_model=schemas.Choice)
def delete_by_id(
        *,
        choice_id: int,
        db: Session = Depends(deps.get_db),
        current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    choice = crud.choice.get(db, id=choice_id)
    if not choice:
        raise HTTPException(
            status_code=404,
            detail="The choice with this choice_id does not exist in the system"
        )
    return crud.choice.remove(db=db, id=choice_id)


@router.put("/{id}", response_model=Any)
def update_choice(
        *,
        db: Session = Depends(deps.get_db),
        choice_id: int,
        choice_in: schemas.ChoiceUpdate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    choice = crud.choice.get(db, id=choice_id)
    if not choice:
        raise HTTPException(
            status_code=404,
            detail="The course with this course_id does not exist in the system"
        )
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=403,
            detail="The user does not have any privileges to access to this page"
        )
    choice = crud.choice.update(db, db_obj=choice, obj_in=choice_in)
    return choice
