from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.Major)
def create_major(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
    major_in: schemas.MajorCreate
) -> Any:
    major = crud.major.create(db, obj_in=major_in)
    return major


@router.get("/", response_model=List[schemas.Major])
def read_majors(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 30,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    majors = crud.major.get_multi(db, skip=skip, limit=limit)
    return majors


@router.delete("/{code}", response_model=schemas.Major)
def delete_by_code(
    *,
    major_code: str,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    major = crud.major.get(db, code=major_code)
    if not major:
        raise HTTPException(
            status_code=404,
            detail="The major with this major_code does not exist in the system"
        )
    return crud.major.remove(db=db, code=major_code)


@router.put("/{code}", response_model=Any)
def update_major(
    *,
    db: Session = Depends(deps.get_db),
    major_code: str,
    major_in: schemas.MajorUpdate,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    major = crud.major.get(db, code=major_code)
    
    if not major:
        raise HTTPException(
            status_code=404,
            detail="The major with this major_code does not exist in the system"
        )
    
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=403,
            detail="The user does not have any privileges to access to this page"
        )
    
    major = crud.major.update(db, db_obj=major, obj_in=major_in)
    return major
