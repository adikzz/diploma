from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.MajorGroup)
def create_major_group(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
    major_group_in: schemas.MajorGroupCreate
) -> Any:
    major_group = crud.major_group.create(db, obj_in=major_group_in)
    return major_group


@router.get("/", response_model=List[schemas.MajorGroup])
def read_major_groups(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 50,
    current_user: models.User = Depends(deps.get_current_active_superuser)
) -> Optional[Any]:
    major_groups = crud.major_group.get_multi(db, skip=skip, limit=limit)
    return major_groups


@router.delete("/{id}", response_model=schemas.MajorGroup)
def delete_by_id(
    *,
    major_group_id: int,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    major_group = crud.major_group.get(db, id=major_group_id)
    if not major_group:
        raise HTTPException(
            status_code=404,
            detail="The major_group with this major_group_id does not exist in the system"
        )
    return crud.major_group.remove(db=db, id=major_group_id)


@router.put("/{id}", response_model=Any)
def update_major_group(
    *,
    db: Session = Depends(deps.get_db),
    major_group_id: int,
    major_group_in: schemas.MajorGroupUpdate,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    major_group = crud.major_group.get(db, id=major_group_id)
    if not major_group:
        raise HTTPException(
            status_code=404,
            detail="The major_group with this major_group_id does not exist in the system"
        )
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=403,
            detail="The user does not have any privileges to access to this page"
        )
    major_group = crud.major_group.update(db, db_obj=major_group, obj_in=major_group_in)
    return major_group
