from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.Group)
def create_group(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
    group_in: schemas.GroupCreate
) -> Any:
    group = crud.group.create(db, obj_in=group_in)
    return group


@router.get("/", response_model=List[schemas.Group])
def read_groups(
        db: Session = Depends(deps.get_db),
        skip: int = 0,
        limit: int = 30
) -> Optional[Any]:
    groups = crud.group.get_multi(db, skip=skip, limit=limit)
    return groups


@router.delete("/{group_name}", response_model=schemas.Group)
def delete_by_id(
    *,
    group_name: str,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    group = crud.group.get(db, name=group_name)
    if not group:
        raise HTTPException(
            status_code=404,
            detail="The group with this group_id does not exist in the system"
        )
    return crud.group.remove(db=db, name=group_name)


@router.put("/{group_name}", response_model=Any)
def update_group(
    *,
    db: Session = Depends(deps.get_db),
    group_name: str,
    group_in: schemas.GroupUpdate,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    group = crud.group.get(db, name=group_name)
    if not group:
        raise HTTPException(
            status_code=404,
            detail="The group with this group_id does not exist in the system"
        )
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=403,
            detail="The user does not have any privileges to access to this page"
        )
    group = crud.group.update(db, db_obj=group, obj_in=group_in)
    return group
