from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.Course)
def create_course(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
    course_in: schemas.CourseCreate
) -> Any:
    course = crud.course.create(db, obj_in=course_in)
    return course


@router.get("/", response_model=List[schemas.Course])
def read_courses(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 30,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Optional[Any]:
    courses = crud.course.get_multi(db, skip=skip, limit=limit)
    return courses


@router.get("/by_groups", response_model=List[schemas.Course])
def read_courses_by_elective_groups(
    group_name: str,
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 30,
    current_user: models.User = Depends(deps.get_current_active_user),
) -> Optional[Any]:
    majors = crud.major_group.get_major_by_group_name(db, group_name=group_name)
    if current_user.major_code in majors:
        return crud.course.get_by_group_name(db, group_name=group_name, skip=skip, limit=limit)



@router.put("/{id}", response_model=Any)
def update_course(
    *,
    db: Session = Depends(deps.get_db),
    course_id: int,
    course_in: schemas.CourseUpdate,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    course = crud.course.get(db, id=course_id)
    if not course:
        raise HTTPException(
            status_code=404,
            detail="The course with this course_id does not exist in the system"
        )
    if not crud.course.is_privileged(current_user):
        raise HTTPException(
            status_code=403,
            detail="The user does not have any privileges to access to this page"
        )
    course = crud.course.update(db, db_obj=course,obj_in=course_in)
    return course


@router.delete("/{id}", response_model=schemas.Course)
def delete_by_id(
    *,
    course_id: int,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    course = crud.course.get(db, id=course_id)
    if not course:
        raise HTTPException(
            status_code=404,
            detail="The course with this course_id does not exist in the system"
        )
    return crud.course.remove(db=db, id=course_id)