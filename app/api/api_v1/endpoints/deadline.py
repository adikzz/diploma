from typing import Any, List, Optional

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.api import deps

router = APIRouter()


@router.post("/", response_model=schemas.Deadline)
def create_deadline(
    *,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
    deadline_in: schemas.DeadlineCreate
) -> Any:
    deadline = crud.deadline.create(db, obj_in=deadline_in)
    return deadline


@router.get("/", response_model=List[schemas.Deadline])
def read_deadlines(
    db: Session = Depends(deps.get_db),
    skip: int = 0,
    limit: int = 30
) -> Optional[Any]:
    deadlines = crud.deadline.get_multi(db, skip=skip, limit=limit)
    return deadlines


@router.delete("/{id}", response_model=schemas.Deadline)
def delete_by_id(
    *,
    deadline_id: int,
    db: Session = Depends(deps.get_db),
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Optional[Any]:
    deadline = crud.deadline.get(db, id=deadline_id)
    if not deadline:
        raise HTTPException(
            status_code=404,
            detail="The deadline with this deadline_id does not exist in the system"
        )
    return crud.deadline.remove(db=db, id=deadline_id)


@router.put("/{id}", response_model=Any)
def update_deadline(
    *,
    db: Session = Depends(deps.get_db),
    deadline_id: int,
    deadline_in: schemas.DeadlineUpdate,
    current_user: models.User = Depends(deps.get_current_active_superuser),
) -> Any:
    deadline = crud.deadline.get(db, id=deadline_id)
    if not deadline:
        raise HTTPException(
            status_code=404,
            detail="The deadline with this deadline_id does not exist in the system"
        )
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=403,
            detail="The user does not have any privileges to access to this page"
        )
    deadline = crud.deadline.update(db, db_obj=deadline, obj_in=deadline_in)
    return deadline
