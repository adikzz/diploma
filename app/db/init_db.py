from sqlalchemy.orm import Session

from app import crud, schemas
from app.core.config import settings

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


def init_db(db: Session) -> None:

    user = crud.user.get_by_email(db, email="test@example.com")
    if not user:
        user_in = schemas.UserCreate(
            email="test@example.com",
            password="test",
            role=schemas.UserRole.ADMIN,
            first_name="Sim",
            last_name="Sim",
            is_active=True,
            is_verified=True
        )
        user = crud.user.create(db, obj_in=user_in)  # noqa: F841
