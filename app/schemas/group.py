from typing import Optional, List

from pydantic import BaseModel

from datetime import datetime


# Shared properties
class GroupBase(BaseModel):
    name: Optional[str]
    description: Optional[str]


# Properties to receive via API on creation
class GroupCreate(GroupBase):
    name: str
    description: str


# Properties to receive via API on update
class GroupUpdate(GroupBase):
    name: Optional[str]
    description: Optional[str]


class GroupInDBBase(GroupBase):

    class Config:
        orm_mode = True


# Additional properties to return via API
class Group(GroupInDBBase):
    pass


# Additional properties stored in DB
class GroupInDB(GroupInDBBase):
    pass