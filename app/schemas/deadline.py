from typing import Optional, List

from pydantic import BaseModel

from datetime import datetime, date


# Shared properties
class DeadlineBase(BaseModel):
    group_name: Optional[str]
    start_time: Optional[datetime]
    end_time: Optional[datetime]
    enrollment_date: Optional[date]


# Properties to receive via API on creation
class DeadlineCreate(DeadlineBase):
    group_name: str
    start_time: datetime
    end_time: datetime
    enrollment_date: date


# Properties to receive via API on update
class DeadlineUpdate(DeadlineBase):
    group_name: Optional[str]
    start_time: Optional[datetime]
    end_time: Optional[datetime]
    enrollment_date: date


class DeadlineInDBBase(DeadlineBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class Deadline(DeadlineInDBBase):
    pass


# Additional properties stored in DB
class DeadlineInDB(DeadlineInDBBase):
    pass