from typing import Optional

from pydantic import BaseModel, EmailStr

from enum import Enum

from datetime import date


class UserRole(str, Enum):
    ADMIN = "ADMIN"
    STUDENT = "STUDENT"

# Shared properties
class UserBase(BaseModel):
    iin: Optional[str]
    second_name: Optional[str]
    first_name: Optional[str]
    middle_name: Optional[str]
    email: Optional[EmailStr]
    group: Optional[str]
    major_code: Optional[str]
    photo_url: Optional[str]
    enrollment_date: Optional[date]


# Properties to receive via API on creation
class UserCreate(UserBase):
    iin: str
    second_name: str
    first_name: str
    middle_name: Optional[str]
    email: EmailStr
    password: str
    group: str
    major_code: str
    photo_url: Optional[str]
    enrollment_date: date
    role: UserRole
    is_active = True
    has_chosen = False


# Properties to receive via API on update
class UserUpdate(UserBase):
    password: Optional[str]


class UserInDBBase(UserBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class User(UserInDBBase):
    pass


# Additional properties stored in DB
class UserInDB(UserInDBBase):
    password: str
