from typing import Optional, List

from pydantic import BaseModel


# Shared properties
class CourseBase(BaseModel):
    name: Optional[str]
    description: Optional[str]
    purpose: Optional[str]
    main_sections: Optional[str]
    prerequisites: Optional[str]
    postrequisites: Optional[str]
    credit_number: Optional[int]
    student_number: Optional[int]
    syllabus: Optional[str]
    teachers: Optional[List[str]]
    group_name: Optional[str]


# Properties to receive via API on creation
class CourseCreate(CourseBase):
    name: str
    description: str
    purpose: str
    main_sections: str
    prerequisites: str
    postrequisites: str
    credit_number: int
    student_number: int
    syllabus: str
    teachers: List[str]
    group_name: str


# Properties to receive via API on update
class CourseUpdate(CourseBase):
    name: Optional[str]
    description: Optional[str]
    purpose: Optional[str]
    main_sections: Optional[str]
    prerequisites: Optional[str]
    postrequisites: Optional[str]
    credit_number: Optional[int]
    student_number: Optional[int]
    syllabus: Optional[str]
    teachers: Optional[List[str]]
    group_name: Optional[str]


class CourseInDBBase(CourseBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class Course(CourseInDBBase):
    pass


# Additional properties stored in DB
class CourseInDB(CourseInDBBase):
    pass