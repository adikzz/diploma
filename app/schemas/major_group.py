from typing import Optional

from pydantic import BaseModel


# Shared properties
class MajorGroupBase(BaseModel):
    major_code: Optional[str]
    group_name: Optional[str]


# Properties to receive via API on creation
class MajorGroupCreate(MajorGroupBase):
    major_code: str
    group_name: str


# Properties to receive via API on update
class MajorGroupUpdate(MajorGroupBase):
    major_code: Optional[str]
    group_name: Optional[str]


class MajorGroupInDBBase(MajorGroupBase):
    id: Optional[int] = None

    class Config:
        orm_mode = True


# Additional properties to return via API
class MajorGroup(MajorGroupInDBBase):
    pass


# Additional properties stored in DB
class MajorGroupInDB(MajorGroupInDBBase):
    pass