from typing import Optional

from pydantic import BaseModel


# Shared properties
class MajorBase(BaseModel):
    code: Optional[str]
    name: Optional[str]


# Properties to receive via API on creation
class MajorCreate(MajorBase):
    code: str
    name: str


# Properties to receive via API on update
class MajorUpdate(MajorBase):
    code: str
    name: str


class MajorInDBBase(MajorBase):

    class Config:
        orm_mode = True


# Additional properties to return via API
class Major(MajorInDBBase):
    pass


# Additional properties stored in DB
class MajorInDB(MajorInDBBase):
    pass