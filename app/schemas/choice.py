from typing import Optional, List

from pydantic import BaseModel

from datetime import datetime


# Shared properties
class ChoiceBase(BaseModel):
    student_id: Optional[int]
    course_id: Optional[int]
    priority: Optional[int]
    group_name: Optional[str]


# Properties to receive via API on creation
class ChoiceCreate(ChoiceBase):
    student_id: int
    course_id: int
    priority: int
    group_name: str


# Properties to receive via API on update
class ChoiceUpdate(ChoiceBase):
    student_id: Optional[int]
    course_id: Optional[int]
    priority: Optional[int]
    group_name: Optional[str]


class ChoiceInDBBase(ChoiceBase):
    id: Optional[int] = None
    choice_date: datetime

    class Config:
        orm_mode = True


# Additional properties to return via API
class Choice(ChoiceInDBBase):
    pass


# Additional properties stored in DB
class ChoiceInDB(ChoiceInDBBase):
    pass